﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using dictionaryApp;
//
//    var dictionaryWord = DictionaryWord.FromJson(jsonString);

namespace dictionaryApp
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;
    

    public partial class DictionaryWord
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class DictionaryWord
    {
        public static DictionaryWord FromJson(string json) => JsonConvert.DeserializeObject<DictionaryWord>(json, dictionaryApp.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this DictionaryWord self) => JsonConvert.SerializeObject(self, dictionaryApp.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
