﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using System.Net.Http;
using ModernHttpClient;
using Newtonsoft.Json;

namespace dictionaryApp
{
    public partial class MainPage : ContentPage
    {
        event ConnectivityChangedEventHandler connectivityChanged;

        public MainPage()
        {
            InitializeComponent();

            CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            {
                if(!CrossConnectivity.Current.IsConnected)
                {
                    
                    Color newColor = new Color(255, 0, 0);
                    status.TextColor = newColor;
                    status.Text = "YOU ARE NOT CONNECTED TO THE INTERNET, UNABLE TO MAKE A SEARCH!";
                    await DisplayAlert("CONNECTION LOST!", "Please regain internet connection to access the dictionary.", "OK");
                }
                else
                {
                    Color newColor = new Color(0, 128, 0);
                    status.TextColor = newColor;

                    var connectionType = CrossConnectivity.Current.ConnectionTypes;
                    status.Text = "YOU ARE CONNECTED VIA: " + CrossConnectivity.Current.ConnectionTypes.First();
                }
                
                
            };

        }

        
        

        async void Entry_Completed(object sender, EventArgs e)
        {
            if (checkInternet())
            {
                Color newColor = new Color(0, 128, 0);
                status.TextColor = newColor;
                

                var connectionType = CrossConnectivity.Current.ConnectionTypes;
                status.Text = "YOU ARE CONNECTED VIA: " + CrossConnectivity.Current.ConnectionTypes.First();    
                status.IsEnabled = true;
                
                var givenWord = ((Entry)sender).Text;

                var client = new HttpClient(new NativeMessageHandler());

                var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + $"{givenWord}"));


                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                DictionaryWord dictionaryData = null;

                HttpResponseMessage response = await client.SendAsync(request);

                if(response.IsSuccessStatusCode)
                {
                    
                    var content = await response.Content.ReadAsStringAsync();

                    if(content.Length > 2)
                    {
                        content = content.Substring(1, content.Length - 2);

                        dictionaryData = DictionaryWord.FromJson(content);

                        type.Text = $"Type: {dictionaryData.Type}";

                        definition.Text = $"Definition: {dictionaryData.Definition}";

                        example.Text = $"Example: {dictionaryData.Example}";
                    }

                }
                else
                {
                    type.Text = "Unable to retrieve information.";
                }

            }
            else
            {
                Color newColor = new Color(255, 0, 0);
                status.TextColor = newColor;
                status.Text = "YOU ARE NOT CONNECTED TO THE INTERNET, UNABLE TO MAKE SEARCH!";
            }
                
        } 

        

        public bool checkInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }

        

        

        


    }
}
